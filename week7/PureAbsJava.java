// Java program to demonstrate pure abstraction using interfaces
import java.util.*;
interface Animal
{
  void sound();
}
class Dog implements Animal
{
  public void sound()
  {
    System.out.println("Bow - Bow!!");
  }
}
class Cat implements Animal
{
  public void sound()
  {
    System.out.println("Meow - Meow!!");
  }
}
public class Main
{
    public static void main(String args[])
    {
     Dog d = new Dog();
     Cat c = new Cat();
     d.sound();
     c.sound();
    }
}
