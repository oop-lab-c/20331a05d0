//parAbsJava
import java.util.*;
abstract class Animal // abstract class
{
  abstract void sound(); // abstract method
  void action()
  {
    System.out.println("Shout");
  }
}
class Dog extends Animal
{
  void sound()
  {
    System.out.println("Bow");
  }
}
class Cat extends Animal
{
  void sound()
  {
    System.out.println("Meow");
  }
}
public class Main
{
    public static void main(String args[])
    {
    Dog d = new Dog();
    Cat c= new Cat();
    d.action();
    d.sound();
    c.action();
    c.sound();
    }
}
