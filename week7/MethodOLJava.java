// MethodOLJava
import java.util.*;
public class Main
{
    int add(int a,int b,int c)
    {
        return a+b+c;
    }
    int add(int a,int b)
    {
        return a+b;
    }
    public static void main(String[] args)
    {
        Main obj = new Main();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the values : ");
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        System.out.println("Addition of 2 numbers : " + obj.add(a,b));
        System.out.println("Addition of 3 numbers : " + obj.add(a,b,c));
    }
}
