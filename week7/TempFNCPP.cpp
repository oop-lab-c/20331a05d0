#include <iostream>
using namespace std;


template <typename T> T Max(T a, T b)
{
    return (a > b) ? a : b;
}

int main()
{
    cout << Max<int>(9, 7) << endl;

    cout << Max<double>(5.9, 4)
         << endl;

    cout << Max<char>('h', 'v')
         << endl;


    return 0;
}
