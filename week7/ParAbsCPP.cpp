#include<iostream>
using namespace std;

class base
{
public:
    virtual void show() {}
};

class derived: public base
{
public:
    void show() { cout << "in derived \n"; }
};

int main(void)
{
    base *bp = new derived();
    bp->show();
    return 0;
}
