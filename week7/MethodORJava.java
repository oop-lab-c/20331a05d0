import java.util.*;
class Parent
{
 void fun()
 {
     System.out.println("Parent class is invoked");
 }
}
class child extends Parent
{
    void fun()
    {
        System.out.println("child class is invoked");
    }
}
public class Main
{
  public static void main(String[] args)
  {
      child obj = new child();
      obj.fun();
  }
}
