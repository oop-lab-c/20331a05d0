#include<iostream>
using namespace std;
class Inh
{
  public:
  int a=10;
  protected:
  int b=20;
  private:
  int c=30;
  public:
  int priv()  //func to access private
  {
      return c;
  }
};
class publicInh : public Inh
{
  public:
  //func to access protected member from base
  int prot()
  {
      return b;
  }
};
class protectedInh : protected Inh {
  public:
    // function to access protected member from Base
    int prot() {
      return b;
    }
    // function to access public member from Base
    int publ() {
      return a;
    }
};
class privateInh : private Inh {
  public:
    // function to access protected member from Base
    int prot() {
      return b;
    }

    // function to access private member
    int publ() {
      return a;
    }
};
int main()
{
  publicInh object1;
  cout << "Private = " << object1.priv() << endl;
  cout << "Protected = " << object1.prot() << endl;
  cout << "Public = " << object1.a<< endl;
  protectedInh object2;
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << object2.prot() << endl;
  cout << "Public = " << object2.publ() << endl;
  privateInh object3;
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << object3.prot() << endl;
  cout << "Public = " << object3.publ() << endl;
  return 0;
}
