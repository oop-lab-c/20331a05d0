interface GrandParent
{
default void display()
{
System.out.println("Hi_____\n");
}
}
interface Parent1 extends GrandParent
{
 default void m1()
 {
     System.out.println("harsha_____\n");
 }
}
interface Parent2 extends GrandParent
{
  default void m2()
  {
      System.out.println("vardhan_____");
  }
}
public class Child implements Parent1, Parent2
{
public static void main(String args[])
{
Child obj = new Child();
obj.display();
obj.m1();
obj.m2();
}
}
