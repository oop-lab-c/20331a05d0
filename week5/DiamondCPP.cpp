#include<iostream>
using namespace std;
class GrdParent
{
    public:
    virtual void displayAge()=0;
};
class Parent1 : public GrdParent
{
    public:
    int par1Age=51;
};
class Parent2 : public GrdParent
{
    public:
    int par2Age=42;
};
class child : public Parent1,Parent2
{
    public:
    int age;
    void setAge()
    {
        age =72;
    }
    void displayAge()
    {
        cout<<age<<endl;
        cout<<par1Age<<endl;
        cout<<par2Age<<endl;
    }
};
int main()
{
    child obj;
    obj.setAge();
    obj.displayAge();
}
