#include<iostream>
using namespace std;
class pq
{
  public:
  void displaypq()
  {
      cout<<"Welcome pq"<<endl;
  }
};
class pqr
{
  public:
  void displaypqr()
  {
      cout<<"Welcome pqr"<<endl;
  }
};
//Simple Inheritance
class chapter1 : public pq
{
    public:
    void displayCh1()
    {
        cout<<"Hi Chap1"<<endl;
    }
};
//Multiple Inheritance
class chapter2 : public pq
{
    public:
    void displayCh2()
    {
        cout<<"Hi Chap2"<<endl;
    }
};
class chapter3 : public chapter1
{
    public:
    void displayCh3()
    {
        cout<<"Hi Chap3"<<endl;
    }
};
class chapter4 : public pqr
{
    public:
    void displayCh4()
    {
        cout<<"Hi Chap4"<<endl;
    }
};
class chapter5 : public pqr
{
    public:
    void displayCh5()
    {
        cout<<"Hi Chap5"<<endl;
    }
};
int main()
{
    chapter1 c1;
    chapter2 c2;
    chapter3 c3;
    chapter4 c4;
    chapter5 c5;
    cout<<"Simple Inheritance"<<endl;
    c1.displaypq();
    cout<<"Multiple Inheritance"<<endl;
    c2.displaypq();
    c2.displayCh2();
    cout<<"Multilevel Inheritance"<<endl;
    c3.displayCh1();
    c3.displayCh3();
    cout<<"Heirarchial Inheritance"<<endl;
    c4.displayCh4();
    c4.displaypqr();
    c5.displayCh5();
    c5.displaypqr();
    return 0;
}
