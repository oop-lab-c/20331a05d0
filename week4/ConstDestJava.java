class Main
{
    public void student()
    {
        String name = "PadalaHarsha";
        double sempercentage = 82.60;
        int rollno = 01;
        String collegename = "MVGR";
        int collegecode = 33;
        System.out.println("Name : "+name);
        System.out.println("SemPercentage : "+sempercentage);
        System.out.println("RollNo : "+rollno);
        System.out.println("CollegeName : "+collegename);
        System.out.println("CollegeCode : "+collegecode);
    }
    protected void finalize()
    {
        System.out.println("Finalize method Invoked");
    }
    public static void main (String[] args) {
        Main obj = new Main();
        obj.student();
        obj.finalize();
    }
}
