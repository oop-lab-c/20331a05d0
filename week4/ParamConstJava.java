import java.util.Scanner;
class Main
{
    public void student()
    {
        String collegename = "MVGR";
        int collegecode = 33;
        System.out.println("CollegeName: "+collegename);
        System.out.println("CollegeCode: "+collegecode);
    }
    public void student(String name , double sempercentage)
    {
        System.out.println("Name: "+name);
        System.out.println("SemPercentage: "+sempercentage);
    }
    protected void finalize()
    {
        System.out.println("Finalize method Invoked");
    }
    public static void main (String[] args) {
        String a;
        double b;
        Scanner f = new Scanner(System.in);
        System.out.println("Enter your name : ");
        a=f.nextLine();
        System.out.println("Enter SemPercentage : ");
        b=f.nextDouble();
        Main obj = new Main();
        obj.student(a,b);
        obj.student();
        obj.finalize();
    }
}
