class AccessSpecifierDemo
{
    public int pubvar;
    protected int provar;
    private int privar;
    void setVar(int pubvar,int provar,int privar)
    {
       this.pubvar=pubvar;
       this.provar=provar;
       this.privar=privar;
    }
    void getVar()
    {
        System.out.println(pubvar);
        System.out.println(provar);
        System.out.println(privar);
    }
    public static void main(String[] args)
    {
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(15,25,35);
        obj.getVar();
    }
}

