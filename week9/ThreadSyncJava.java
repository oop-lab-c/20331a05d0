class Table{
    synchronized void printTable(int n)
    {
        for(int i=1;i<=8;i++)
        {
            System.out.println(n*i);

        }

    }
   }

class MyThread1 extends Thread
{
    Table t;
    MyThread1(Table t)
    {
        this.t=t;
    }
    public void run()
    {
        t.printTable(7);
    }

}
class MyThread2 extends Thread
{
    Table t;
    MyThread2(Table t)
    {
        this.t=t;
    }
    public void run()
    {
        t.printTable(50);
    }
}

public class Main
{
    public static void main(String args[])
    {
        Table obj = new Table();
        MyThread1 t1=new MyThread1(obj);
        MyThread2 t2=new MyThread2(obj);
        t1.start();
        t2.start();
    }
}
